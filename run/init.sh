# 
# This file is part of the SecureFlag Platform.
# Copyright (c) 2020 SecureFlag Limited.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

echo "Starting SecureFlag Database CE Service..."
chmod +x /tmp/mysql.sh
/tmp/mysql.sh mysqld &
sleep 50
echo "configuring databases"

DBNAME=guacamole_db
mysql -fu root -p$MYSQL_ROOT_PASSWORD --batch --skip-column-names -e "SHOW DATABASES LIKE '"$DBNAME"';" | grep "$DBNAME" > /dev/null
if [ $? -eq 0 ];then
	echo "guacamole_db DATABASE ALREADY EXISTS" 
else
    echo "guacamole_db DATABASE DOES NOT EXIST"
	mysql -fu root -p$MYSQL_ROOT_PASSWORD -e "CREATE USER IF NOT EXISTS 'guacamole_user'@'%' IDENTIFIED BY '$MYSQL_GUAC_PASSWORD';"
	mysql -fu root -p$MYSQL_ROOT_PASSWORD < /tmp/sql/init_guac.sql
	mysql -fu guacamole_user guacamole_db -p$MYSQL_GUAC_PASSWORD < /tmp/sql/guac-create-schema.sql
	mysql -fu guacamole_user guacamole_db -p$MYSQL_GUAC_PASSWORD -e "INSERT INTO guacamole_user(username, password_hash, password_salt, password_date) VALUES ('sfadmin',UNHEX(SHA2(CONCAT('$GUAC_ADMIN_PASSWORD','EA38DE4168AF9C99E9CDD77F7AB07E8202554894A50F1B1334CD2579BD9714EA'),256)),x'EA38DE4168AF9C99E9CDD77F7AB07E8202554894A50F1B1334CD2579BD9714EA',NOW());"
	mysql -fu guacamole_user guacamole_db -p$MYSQL_GUAC_PASSWORD < /tmp/sql/guac-create-admin-user.sql
fi

DBNAME=dbPortalCE
mysql -fu root -p$MYSQL_ROOT_PASSWORD --batch --skip-column-names -e "SHOW DATABASES LIKE '"$DBNAME"';" | grep "$DBNAME" > /dev/null
if [ $? -eq 0 ];then
	echo "dbPortalCE DATABASE ALREADY EXISTS" 
else
	echo "dbPortalCE DATABASE DOES NOT EXIST"

	REG=`echo "$GATEWAY_REGION" | awk '{print toupper($0)}' | sed -e 's/-/_/g'`

	mysql -fu root -p$MYSQL_ROOT_PASSWORD -e "CREATE USER IF NOT EXISTS 'PortalDBUser'@'%' IDENTIFIED BY '$MYSQL_PORTAL_PASSWORD';"
	mysql -fu root -p$MYSQL_ROOT_PASSWORD < /tmp/sql/init_global.sql
	mysql -fu PortalDBUser dbPortalCE -p$MYSQL_PORTAL_PASSWORD -e "INSERT INTO users VALUES (3,NULL,-1,'admin@changeme.com',1,0,NULL,'admin',1,1,NULL,'2020-04-01 00:00:01',NULL,'sf',SHA2(CONCAT('$PORTAL_ADMIN_PASSWORD','x6chJMlRu8er6ddV6GBSTQ=='),512),NULL,NULL,-1,'x6chJMlRu8er6ddV6GBSTQ==',0,'ACTIVE','admin',79,1,3);"
	mysql -fu PortalDBUser dbPortalCE -p$MYSQL_PORTAL_PASSWORD -e "INSERT INTO users_organizations VALUES (3,1);"
	mysql -fu PortalDBUser dbPortalCE -p$MYSQL_PORTAL_PASSWORD -e "INSERT INTO Gateways VALUES (1,1,0,1,'$GATEWAY_FQDN','Exercise Gateway',0,'$REG','AVAILABLE');"
fi

if [ ! -z "$SIGNAL_URL" ]; then
	echo "INFO: Notifying CloudFormation $SIGNAL_URL"
	curl -k -X PUT -H 'Content-Type:' --data-binary '{"Status" : "SUCCESS","Reason" : "Mysql Configuration Complete","UniqueId" : "ID1337","Data" : "Mysql has completed configuration."}' $SIGNAL_URL
	echo "INFO: Notified CloudFormation"
fi
echo "Setting MAX Packet to 1G"
mysql -fu root -p$MYSQL_ROOT_PASSWORD -e "SET GLOBAL max_allowed_packet=1073741824;"
echo "Setting MAX Connections to 250"
mysql -fu root -p$MYSQL_ROOT_PASSWORD -e "SET GLOBAL max_connections=250;"

rm /tmp/init.sh
tail -f /var/log/mysql/error.log
