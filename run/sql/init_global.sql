-- 
-- This file is part of the SecureFlag Platform.
-- Copyright (c) 2020 SecureFlag Limited.
-- 
-- This program is free software: you can redistribute it and/or modify  
-- it under the terms of the GNU General Public License as published by  
-- the Free Software Foundation, version 3.
--
-- This program is distributed in the hope that it will be useful, but 
-- WITHOUT ANY WARRANTY; without even the implied warranty of 
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License 
-- along with this program. If not, see <http://www.gnu.org/licenses/>.
--


CREATE DATABASE  IF NOT EXISTS `dbPortalCE` /*!40100 DEFAULT CHARACTER SET latin1 */;
GRANT ALL ON dbPortalCE.* TO 'PortalDBUser'@'%';
FLUSH PRIVILEGES;
USE `dbPortalCE`;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `idCountry` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `short` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCountry`),
  UNIQUE KEY `UK_1pyiwrqimi3hnl3vtgsypj5r` (`name`),
  UNIQUE KEY `UK_abemnrkmvcivl2tcwd618c0jr` (`short`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`idCountry`, `name`, `short`)
VALUES
  (1,'Andorra','AD'),
  (2,'United Arab Emirates','AE'),
  (3,'Afghanistan','AF'),
  (4,'Antigua and Barbuda','AG'),
  (5,'Anguilla','AI'),
  (6,'Albania','AL'),
  (7,'Armenia','AM'),
  (8,'Netherlands Antilles','AN'),
  (9,'Angola','AO'),
  (10,'Antarctica','AQ'),
  (11,'Argentina','AR'),
  (12,'American Samoa','AS'),
  (13,'Austria','AT'),
  (14,'Australia','AU'),
  (15,'Aruba','AW'),
  (16,'Aland Islands','AX'),
  (17,'Azerbaijan','AZ'),
  (18,'Bosnia and Herzegovina','BA'),
  (19,'Barbados','BB'),
  (20,'Bangladesh','BD'),
  (21,'Belgium','BE'),
  (22,'Burkina Faso','BF'),
  (23,'Bulgaria','BG'),
  (24,'Bahrain','BH'),
  (25,'Burundi','BI'),
  (26,'Benin','BJ'),
  (27,'Saint Barthélemy','BL'),
  (28,'Bermuda','BM'),
  (29,'Brunei','BN'),
  (30,'Bolivia','BO'),
  (31,'Bonaire','BQ'),
  (32,'Brazil','BR'),
  (33,'Bahamas','BS'),
  (34,'Bhutan','BT'),
  (35,'Bouvet Island','BV'),
  (36,'Botswana','BW'),
  (37,'Belarus','BY'),
  (38,'Belize','BZ'),
  (39,'Canada','CA'),
  (40,'Cocos Islands','CC'),
  (41,'Democratic Republic of the Congo','CD'),
  (42,'Central African Republic','CF'),
  (43,'Republic of the Congo','CG'),
  (44,'Switzerland','CH'),
  (45,'Ivory Coast','CI'),
  (46,'Cook Islands','CK'),
  (47,'Chile','CL'),
  (48,'Cameroon','CM'),
  (49,'China','CN'),
  (50,'Colombia','CO'),
  (51,'Costa Rica','CR'),
  (52,'Serbia and Montenegro','CS'),
  (53,'Cuba','CU'),
  (54,'Cape Verde','CV'),
  (55,'Curaçao','CW'),
  (56,'Christmas Island','CX'),
  (57,'Cyprus','CY'),
  (58,'Czech Republic','CZ'),
  (59,'Germany','DE'),
  (60,'Djibouti','DJ'),
  (61,'Denmark','DK'),
  (62,'Dominica','DM'),
  (63,'Dominican Republic','DO'),
  (64,'Algeria','DZ'),
  (65,'Ecuador','EC'),
  (66,'Estonia','EE'),
  (67,'Egypt','EG'),
  (68,'Western Sahara','EH'),
  (69,'Eritrea','ER'),
  (70,'Spain','ES'),
  (71,'Ethiopia','ET'),
  (72,'Finland','FI'),
  (73,'Fiji','FJ'),
  (74,'Falkland Islands','FK'),
  (75,'Micronesia','FM'),
  (76,'Faroe Islands','FO'),
  (77,'France','FR'),
  (78,'Gabon','GA'),
  (79,'United Kingdom','GB'),
  (80,'Grenada','GD'),
  (81,'Georgia','GE'),
  (82,'French Guiana','GF'),
  (83,'Guernsey','GG'),
  (84,'Ghana','GH'),
  (85,'Gibraltar','GI'),
  (86,'Greenland','GL'),
  (87,'Gambia','GM'),
  (88,'Guinea','GN'),
  (89,'Guadeloupe','GP'),
  (90,'Equatorial Guinea','GQ'),
  (91,'Greece','GR'),
  (92,'South Georgia and the South Sandwich Islands','GS'),
  (93,'Guatemala','GT'),
  (94,'Guam','GU'),
  (95,'Guinea-Bissau','GW'),
  (96,'Guyana','GY'),
  (97,'Hong Kong','HK'),
  (98,'Heard Island and McDonald Islands','HM'),
  (99,'Honduras','HN'),
  (100,'Croatia','HR'),
  (101,'Haiti','HT'),
  (102,'Hungary','HU'),
  (103,'Indonesia','ID'),
  (104,'Ireland','IE'),
  (105,'Israel','IL'),
  (106,'Isle of Man','IM'),
  (107,'India','IN'),
  (108,'British Indian Ocean Territory','IO'),
  (109,'Iraq','IQ'),
  (110,'Iran','IR'),
  (111,'Iceland','IS'),
  (112,'Italy','IT'),
  (113,'Jersey','JE'),
  (114,'Jamaica','JM'),
  (115,'Jordan','JO'),
  (116,'Japan','JP'),
  (117,'Kenya','KE'),
  (118,'Kyrgyzstan','KG'),
  (119,'Cambodia','KH'),
  (120,'Kiribati','KI'),
  (121,'Comoros','KM'),
  (122,'Saint Kitts and Nevis','KN'),
  (123,'North Korea','KP'),
  (124,'South Korea','KR'),
  (125,'Kuwait','KW'),
  (126,'Cayman Islands','KY'),
  (127,'Kazakhstan','KZ'),
  (128,'Laos','LA'),
  (129,'Lebanon','LB'),
  (130,'Saint Lucia','LC'),
  (131,'Liechtenstein','LI'),
  (132,'Sri Lanka','LK'),
  (133,'Liberia','LR'),
  (134,'Lesotho','LS'),
  (135,'Lithuania','LT'),
  (136,'Luxembourg','LU'),
  (137,'Latvia','LV'),
  (138,'Libya','LY'),
  (139,'Morocco','MA'),
  (140,'Monaco','MC'),
  (141,'Moldova','MD'),
  (142,'Montenegro','ME'),
  (143,'Saint Martin','MF'),
  (144,'Madagascar','MG'),
  (145,'Marshall Islands','MH'),
  (146,'Macedonia','MK'),
  (147,'Mali','ML'),
  (148,'Myanmar','MM'),
  (149,'Mongolia','MN'),
  (150,'Macao','MO'),
  (151,'Northern Mariana Islands','MP'),
  (152,'Martinique','MQ'),
  (153,'Mauritania','MR'),
  (154,'Montserrat','MS'),
  (155,'Malta','MT'),
  (156,'Mauritius','MU'),
  (157,'Maldives','MV'),
  (158,'Malawi','MW'),
  (159,'Mexico','MX'),
  (160,'Malaysia','MY'),
  (161,'Mozambique','MZ'),
  (162,'Namibia','NA'),
  (163,'New Caledonia','NC'),
  (164,'Niger','NE'),
  (165,'Norfolk Island','NF'),
  (166,'Nigeria','NG'),
  (167,'Nicaragua','NI'),
  (168,'Netherlands','NL'),
  (169,'Norway','NO'),
  (170,'Nepal','NP'),
  (171,'Nauru','NR'),
  (172,'Niue','NU'),
  (173,'New Zealand','NZ'),
  (174,'Oman','OM'),
  (175,'Panama','PA'),
  (176,'Peru','PE'),
  (177,'French Polynesia','PF'),
  (178,'Papua New Guinea','PG'),
  (179,'Philippines','PH'),
  (180,'Pakistan','PK'),
  (181,'Poland','PL'),
  (182,'Saint Pierre and Miquelon','PM'),
  (183,'Pitcairn','PN'),
  (184,'Puerto Rico','PR'),
  (185,'Palestinian Territory','PS'),
  (186,'Portugal','PT'),
  (187,'Palau','PW'),
  (188,'Paraguay','PY'),
  (189,'Qatar','QA'),
  (190,'Reunion','RE'),
  (191,'Romania','RO'),
  (192,'Serbia','RS'),
  (193,'Russia','RU'),
  (194,'Rwanda','RW'),
  (195,'Saudi Arabia','SA'),
  (196,'Solomon Islands','SB'),
  (197,'Seychelles','SC'),
  (198,'Sudan','SD'),
  (199,'Sweden','SE'),
  (200,'Singapore','SG'),
  (201,'Saint Helena','SH'),
  (202,'Slovenia','SI'),
  (203,'Svalbard and Jan Mayen','SJ'),
  (204,'Slovakia','SK'),
  (205,'Sierra Leone','SL'),
  (206,'San Marino','SM'),
  (207,'Senegal','SN'),
  (208,'Somalia','SO'),
  (209,'Suriname','SR'),
  (210,'South Sudan','SS'),
  (211,'Sao Tome and Principe','ST'),
  (212,'El Salvador','SV'),
  (213,'Sint Maarten','SX'),
  (214,'Syria','SY'),
  (215,'Swaziland','SZ'),
  (216,'Turks and Caicos Islands','TC'),
  (217,'Chad','TD'),
  (218,'French Southern Territories','TF'),
  (219,'Togo','TG'),
  (220,'Thailand','TH'),
  (221,'Tajikistan','TJ'),
  (222,'Tokelau','TK'),
  (223,'East Timor','TL'),
  (224,'Turkmenistan','TM'),
  (225,'Tunisia','TN'),
  (226,'Tonga','TO'),
  (227,'Turkey','TR'),
  (228,'Trinidad and Tobago','TT'),
  (229,'Tuvalu','TV'),
  (230,'Taiwan','TW'),
  (231,'Tanzania','TZ'),
  (232,'Ukraine','UA'),
  (233,'Uganda','UG'),
  (234,'United States Minor Outlying Islands','UM'),
  (235,'United States','US'),
  (236,'Uruguay','UY'),
  (237,'Uzbekistan','UZ'),
  (238,'Vatican','VA'),
  (239,'Saint Vincent and the Grenadines','VC'),
  (240,'Venezuela','VE'),
  (241,'British Virgin Islands','VG'),
  (242,'U.S. Virgin Islands','VI'),
  (243,'Vietnam','VN'),
  (244,'Vanuatu','VU'),
  (245,'Wallis and Futuna','WF'),
  (246,'Samoa','WS'),
  (247,'Kosovo','XK'),
  (248,'Yemen','YE'),
  (249,'Mayotte','YT'),
  (250,'South Africa','ZA'),
  (251,'Zambia','ZM'),
  (252,'Zimbabwe','ZW');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# table Gateways
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Gateways`;

CREATE TABLE `Gateways` (
  `idGateway` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `enableImageSync` bit(1) DEFAULT b'1',
  `enableScaleIn` bit(1) DEFAULT b'1',
  `fqdn` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `placementStrategy` int(11) DEFAULT '0',
  `region` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idGateway`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# table organizations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organizations`;

CREATE TABLE `organizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allowManualReview` bit(1) DEFAULT b'1',
  `createdByUser` int(11) DEFAULT NULL,
  `dateJoined` datetime(6) DEFAULT NULL,
  `defaultCredits` int(11) DEFAULT '10',
  `email` varchar(255) DEFAULT NULL,
  `maxUsers` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_p9pbw3flq9hkay8hdx3ypsldy` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `organizations` WRITE;
/*!40000 ALTER TABLE `organizations` DISABLE KEYS */;

INSERT INTO `organizations` (`id`, `allowManualReview`, `createdByUser`, `dateJoined`, `defaultCredits`, `email`, `maxUsers`, `name`, `status`)
VALUES
  (1,b'1',NULL,'2020-04-01 00:00:01.000000',-1,'test@changeme.com',100,'Org',1);

/*!40000 ALTER TABLE `organizations` ENABLE KEYS */;
UNLOCK TABLES;


# table supportedAWSRegions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supportedAWSRegions`;

CREATE TABLE `supportedAWSRegions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `supportedAWSRegions` WRITE;
/*!40000 ALTER TABLE `supportedAWSRegions` DISABLE KEYS */;

INSERT INTO `supportedAWSRegions` (`id`, `code`, `name`)
VALUES
  (1,'US_EAST_2','US East (Ohio)'),
  (2,'US_EAST_1','US East (N. Virginia)'),
  (3,'US_WEST_2','US West (Oregon)'),
  (4,'US_WEST_1','US West (N. California)'),
  (5,'CA_CENTRAL_1','Canada (Central)'),
  (6,'EU_CENTRAL_1','EU (Frankfurt)'),
  (7,'EU_WEST_1','EU (Ireland)'),
  (8,'EU_WEST_2','EU (London)'),
  (9,'EU_WEST_3','EU (Paris)'),
  (10,'EU_NORTH_1','EU (Stockholm)'),
  (11,'ME_SOUTH_1','Middle East (Bahrain)'),
  (12,'AP_NORTHEAST_2','Asia Pacific (Seoul)'),
  (13,'AP_NORTHEAST_1','Asia Pacific (Tokyo)'),
  (14,'AP_SOUTHEAST_2','Asia Pacific (Sydney)'),
  (15,'AP_SOUTHEAST_1','Asia Pacific (Singapore)'),
  (16,'SA_EAST_1','South America (São Paulo)'),
  (17,'AP_SOUTH_1','Asia Pacific (Mumbai)'),
  (18,'AP_EAST_1','Asia Pacific (Hong Kong)');
/*!40000 ALTER TABLE `supportedAWSRegions` ENABLE KEYS */;
UNLOCK TABLES;


# table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `idTeam` int(11) NOT NULL AUTO_INCREMENT,
  `createdByUser` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `organizationId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTeam`),
  KEY `FKpmh6uy0ah6a1lfkpwt3uf42ge` (`organizationId`),
  CONSTRAINT `FKpmh6uy0ah6a1lfkpwt3uf42ge` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`idTeam`, `createdByUser`, `name`, `organizationId`)
VALUES
  (3,NULL,'Team A',1);

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# table teams_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams_users`;

CREATE TABLE `teams_users` (
  `Team_idTeam` int(11) NOT NULL,
  `managers_idUser` int(11) NOT NULL,
  PRIMARY KEY (`Team_idTeam`,`managers_idUser`),
  KEY `FKn60lmp4tg0tp32tvvopre4rer` (`managers_idUser`),
  CONSTRAINT `FKb7vywynxg8hrtiaxgsaek9u3c` FOREIGN KEY (`Team_idTeam`) REFERENCES `teams` (`idTeam`),
  CONSTRAINT `FKn60lmp4tg0tp32tvvopre4rer` FOREIGN KEY (`managers_idUser`) REFERENCES `users` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `createdByUser` int(11) DEFAULT NULL,
  `credits` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `emailVerified` bit(1) DEFAULT NULL,
  `exercisesRun` int(11) NOT NULL,
  `expirationDateTime` datetime(6) DEFAULT NULL,
  `firstName` varchar(255) NOT NULL,
  `forceChangePassword` bit(1) DEFAULT NULL,
  `instanceLimit` int(11) NOT NULL,
  `invitationCodeRedeemed` varchar(255) DEFAULT NULL,
  `joinDateTime` datetime(6) DEFAULT NULL,
  `lastLogin` datetime(6) DEFAULT NULL,
  `lastName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `personalDataAnonymisedDateTime` datetime(6) DEFAULT NULL,
  `personalDataUpdateDateTime` datetime(6) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `score` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `countryId` int(11) DEFAULT NULL,
  `defaultOrganizationId` int(11) DEFAULT NULL,
  `teamId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `UK_r43af9ap4edm43mmtq01oddj6` (`username`),
  KEY `FKofy65v1xgu1ikcyjwd8g5tppu` (`countryId`),
  KEY `FKp5dw3mvjorsdwo6e8w6rwiytk` (`defaultOrganizationId`),
  KEY `FKhimxffv2tyfh6dkhfh4wsdds8` (`teamId`),
  CONSTRAINT `FKhimxffv2tyfh6dkhfh4wsdds8` FOREIGN KEY (`teamId`) REFERENCES `teams` (`idTeam`),
  CONSTRAINT `FKofy65v1xgu1ikcyjwd8g5tppu` FOREIGN KEY (`countryId`) REFERENCES `countries` (`idCountry`),
  CONSTRAINT `FKp5dw3mvjorsdwo6e8w6rwiytk` FOREIGN KEY (`defaultOrganizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# table users_organizations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_organizations`;

CREATE TABLE `users_organizations` (
  `User_idUser` int(11) NOT NULL,
  `managedOrganizations_id` int(11) NOT NULL,
  PRIMARY KEY (`User_idUser`,`managedOrganizations_id`),
  KEY `FKqruontmt4w0qhlf7klmml6g7s` (`managedOrganizations_id`),
  CONSTRAINT `FKf3hpovyxjl921trmk44s5jtu4` FOREIGN KEY (`User_idUser`) REFERENCES `users` (`idUser`),
  CONSTRAINT `FKqruontmt4w0qhlf7klmml6g7s` FOREIGN KEY (`managedOrganizations_id`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;