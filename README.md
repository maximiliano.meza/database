# SecureFlag CE Database 

This repository is part of the SecureFlag Community platform.

Please refer to the official [documentation](https://community.secureflag.com) for further information and installation instructions.