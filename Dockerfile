# 
# This file is part of the SecureFlag Platform.
# Copyright (c) 2020 SecureFlag Limited.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

FROM debian:buster-slim
ENV MYSQL_MAJOR 5.7
ENV MYSQL_VERSION 5.7.31-1debian10
ENV GOSU_VERSION 1.7

RUN groupadd -r mysql && useradd -r -g mysql mysql \ 
	&& apt-get update && apt-get install -y --no-install-recommends gnupg nfs-common dirmngr pwgen openssl perl python=2.7.* python-pip \
    && apt-get -y install python-setuptools wget unzip \	
    && pip install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz \
    && apt-get -y purge python-pip \
    && apt-get -y autoremove \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/* \
	&& set -x \
	&& apt-get update && apt-get install -y --no-install-recommends ca-certificates wget curl && rm -rf /var/lib/apt/lists/* \
	&& wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)" \
	&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc" \
	&& export GNUPGHOME="$(mktemp -d)" \
	&& gpg --keyserver ipv4.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
	&& gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
	&& rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc \
	&& chmod +x /usr/local/bin/gosu \
	&& gosu nobody true \
	&& mkdir /docker-entrypoint-initdb.d \
	&& set -ex; \
		key='A4A9406876FCBD3C456770C88C718D3B5072E1F5'; \
		export GNUPGHOME="$(mktemp -d)"; \
		gpg --keyserver ipv4.pool.sks-keyservers.net --recv-keys "$key"; \
		gpg --export "$key" > /etc/apt/trusted.gpg.d/mysql.gpg; \
		rm -rf "$GNUPGHOME"; \
		apt-key list > /dev/null \
	&& apt-get purge -y --auto-remove ca-certificates wget \
	&& echo "deb http://repo.mysql.com/apt/debian/ buster mysql-${MYSQL_MAJOR}" > /etc/apt/sources.list.d/mysql.list \
	&& { \
			echo mysql-community-server mysql-community-server/data-dir select ''; \
			echo mysql-community-server mysql-community-server/root-pass password ''; \
			echo mysql-community-server mysql-community-server/re-root-pass password ''; \
			echo mysql-community-server mysql-community-server/remove-test-db select false; \
		} | debconf-set-selections \
	&& apt-get update && apt-get install -y --allow-unauthenticated mysql-server="${MYSQL_VERSION}" \
	&& rm -rf /var/lib/apt/lists/* \
	&& rm -rf /var/lib/mysql && mkdir -p /var/lib/mysql /var/run/mysqld \
	&& chown -R mysql:mysql /var/lib/mysql /var/run/mysqld \
	&& chmod 777 /var/run/mysqld \
	&& find /etc/mysql/ -name '*.cnf' -print0 \
		| xargs -0 grep -lZE '^(bind-address|log)' \
		| xargs -rt -0 sed -Ei 's/^(bind-address|log)/#&/' \
	&& echo '[mysqld]\nskip-host-cache\nskip-name-resolve' > /etc/mysql/conf.d/docker.cnf \
	&& apt-get autoremove && apt-get clean

VOLUME /var/lib/mysql
COPY	run/ /tmp/run/
RUN     mv /tmp/run/init.sh /tmp/init.sh \ 
		&& mv /tmp/run/mysql.sh /tmp/mysql.sh \
		&& mv /tmp/run/sql /tmp/sql \
		&& rm -rf /tmp/run \
		&& chmod +x /tmp/init.sh && chmod +x /tmp/mysql.sh && chmod +x /usr/local/bin/gosu

EXPOSE 3306
ENTRYPOINT /tmp/init.sh